from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('w1.views',
     url(r'^(?P<oid>\d+)/$', 'payment_form'),
     url(r'^process/$', 'process'),
)
